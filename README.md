# Projet pour discuter des bonnes pratiques d'utilisation de git au boulot ! 

## Workflow - Ce qu'il faut retenir
- Il y a une hiérarchisation des branches : donc des branches qui se retrouvent être plus sensibles que d'autres.

Pour contribuer a une branche "sensible", il est conseillé de demander une revue de code a un collègue ou au moins d'en discuter 
**=> Faire des merges request ou valider au cours de pair programming ou si le changement est petit**
Attention : Une branche sensible est une branche a partir de laquelle vos collègues pourraient partir : Exemple develop (branche de développement projet) mais également la branche d'une grosse feature découpée en taches

- Faire des branches et des push fréquents : profiter au maximum d'un déploiement rapide du code, avec du test en continu qui est fait a distance ce qui permet d'être au clair pour la suite.

- Préparer des US en amont avec les merge requests : Une US -> Une problématique donc une merge request. On peut se dire qu'on lance une merge request en WIP (pour ne pas pouvoir la valider), qu'on crée ensuite une branche qui correspondra exactement au besoin et qu'on travaille sur des branches filles de celle ci pour la bonne résolution de la problématique.


## Travailler avec le gitflow

Toujours se déplacer sur les branches les plus stables pour récupérer les mises a jour : 
```
git checkout master
git pull
```
puis partir de la où l'on veut travailler : Soit d'une branche d'US soit de dévelop si on fait une petite tache qui est indépendante.
```
git checkout dv 
git checkout -b "ma-nouvelle-branche"
```
faire des changements en local puis
```
git add . && git commit -m "stuff" && git push
```
(NB : on peut travailler en local en amendant le commit pour une belle histoire sur cette branche indépendante, mais le fonctionnement de merge avec squash rend un peu caduque cet usage)
a répéter autant de fois que cela semble nécessaire

puis l'on faisait souvent : 
```
git rebase -i ma-nouvelle-branche
``` 

ce qui ouvrait : 
```
r fb554f5 Feature
s 2bd1903 This is commit 2
s d987ebf This is commit 3

# Rebase 9cbc329..d987ebf onto 9cbc329
#
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
#
# If you remove a line here THAT COMMIT WILL BE LOST.
# However, if you remove everything, the rebase will be aborted.
#
```

Ce qui mélangeait tous les commits sur le dépot distant sur la branche en un "FEATURE".
Que l'on mergeait par la suite (sans forcément se concerter

```
git checkout dv 
git pull
git merge ma-nouvelle-branche
(potentiellement des conflits)
git push 
```

## Avec gitlab

Avec gitlab, on a un fonctionnement transparent jusqu'a la période qui nous embête : le rebase .
On fait un rebase squash directement par le gestionnaire en ligne avec les merge requests !
Il suffit du coup de proposer une merge request a validation.
Ce qui est cool c'est que c'est transparent dans une contribution dans un projet interne ou vers les autres projets ! Ce qui du coup permet de mutualiser les pratiques (on ne peut pas merge normalement en interne comme ça, ce qui entraine forcément plus de complexité)

Ces merges requests proposent un canal de discussion sur le sujet, une historicité des échanges sur l'ajout de la fonctionnalité (utile pour l'avenir) et aussi une lisibilité claire (comme un commit) de tous les changements, avec potentiel de commenter et éditer  toutes les lignes de code depuis le site de gitlab.

Fonctionnalité plus avancée : 
On peut relier ses merges requests a des ISSUES qui du coup sont des bugs / demandes de fonctionnalités coté utilisateur. ça donne une meilleure lisibilité pour le projet de la part de l'équipe info et stat sur ce qui se fait, et sur si c'est un problème en cours d'investigation.

